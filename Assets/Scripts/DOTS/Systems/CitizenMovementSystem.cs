﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Mathematics.Extensions;
using Unity.Transforms;
using UnityEngine;

namespace UrbanWorld.Pathfinding
{
    [DisableAutoCreation]
    public class CitizenMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var timedelta = Time.DeltaTime;

            Entities
                .ForEach((
                Entity e,
                int entityInQueryIndex,
                DynamicBuffer<PathPositionsBuffer> pathPositionsBuffer,
                ref PathFollowComponent follow,
                ref Translation position,
                ref Rotation rotation,
                in CitizenComponent citizenData) =>
                {
                    if (follow.pathIndex >= 0)
                    {
                        float3 target = pathPositionsBuffer[follow.pathIndex].position;
                        float3 desiredPos = Float3Extensions.MoveTowards(position.Value, target, citizenData.speed * timedelta);
                        position.Value = desiredPos;
                        if (math.length(target - position.Value) != 0)
                            rotation.Value = quaternion.LookRotation(target - position.Value, new float3(0, 1, 0));

                        if (math.distance(position.Value, target) == 0)
                        {
                            follow.pathIndex--;
                        }
                    }
                }).ScheduleParallel();
        }
    }
}