﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Mathematics.Extensions;
using Unity.Transforms;
using UnityEngine;
using UrbanWorld.Generators;
using UnityRandom = UnityEngine.Random;

namespace UrbanWorld.Pathfinding
{
    [DisableAutoCreation]
    public class SpawnerSystem : SystemBase
    {
        public float3[] vehicleStartLocations;
        public float3[] citizenStartLocations;
        public NodeOOP[,] nodesOOP;
        public GameObject[] citizensPrefabsOriginal;
        public GameObject[] vehiclePrefabsOriginal;

        public int citizensTotal { get; private set; } = 0;
        public int vehiclesTotal { get; private set; } = 0;

        private BlobAssetStore blobAssetStore;
        private GameObjectConversionSettings settings;
        private EntityManager manager;

        public void BindCitizenPrefabs(GameObject[] citizensPrefabs)
        {
            citizensPrefabsOriginal = citizensPrefabs;
        }
        public void BindVehiclesPrefabs(GameObject[] vehiclesPrefabs)
        {
            vehiclePrefabsOriginal = vehiclesPrefabs;
        }

        protected override void OnCreate()
        {
            blobAssetStore = new BlobAssetStore();
            settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore);
            manager = World.DefaultGameObjectInjectionWorld.EntityManager;
        }

        protected override void OnStartRunning()
        {
            citizensTotal = 0;
            vehiclesTotal = 0;

            Entities.WithoutBurst().ForEach((in SpawnerComponent spawner) =>
            {
                citizensTotal += spawner.citizensToSpawn;
                vehiclesTotal += spawner.vehiclesToSpawn;
            }).Run();

            citizensTotal /= 10;
            vehiclesTotal *= 3;

            for (int i = 0; i < citizensTotal; i++)
            {
                Entity spawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(citizensPrefabsOriginal[UnityRandom.Range(0, citizensPrefabsOriginal.Length)], settings);
                var instanceCitizen = manager.Instantiate(spawn);

                float3 desiredPos = citizenStartLocations[UnityRandom.Range(0, citizenStartLocations.Length)];

                manager.SetComponentData(
                    instanceCitizen,
                    new Translation { Value = desiredPos });
            }
            for (int i = 0; i < vehiclesTotal; i++)
            {
                Entity spawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(vehiclePrefabsOriginal[UnityRandom.Range(0, vehiclePrefabsOriginal.Length)], settings);
                var instanceVehicle = manager.Instantiate(spawn);
                float3 desiredPos = vehicleStartLocations[UnityRandom.Range(0, vehicleStartLocations.Length)];
                manager.SetComponentData(
                    instanceVehicle,
                    new Translation { Value = desiredPos });
            }
        }

        protected override void OnUpdate()
        {

        }

        protected override void OnStopRunning()
        {
            var entitiesCol = EntityManager.GetAllEntities();
            for (int i = 0; i < entitiesCol.Length; i++)
            {
                if (EntityManager.HasComponent<RequestSceneLoaded>(entitiesCol[i]))
                    continue;
                EntityManager.DestroyEntity(entitiesCol[i]);
            }
            entitiesCol.Dispose();
        }

        protected override void OnDestroy()
        {
            blobAssetStore.Dispose();
        }
    }
}