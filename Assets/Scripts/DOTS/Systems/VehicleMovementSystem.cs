﻿using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Mathematics.Extensions;
using Unity.Transforms;

namespace UrbanWorld.Pathfinding
{
    [DisableAutoCreation]
    public class VehicleMovementSystem : SystemBase
    {
        private RoadConnectionData[] roadConnections;

        public void BindRoadConnectionsData(HashSet<RoadNetwork.TrafficRoute> pulledRoadConnections)
        {
            roadConnections = new RoadConnectionData[pulledRoadConnections.Count];
            int i = 0;
            foreach (var route in pulledRoadConnections)
            {
                roadConnections[i] = new RoadConnectionData
                {
                    startPosition = route.startPosition,
                    endPosition = route.endPosition
                };
                i++;
            }
        }

        protected override void OnUpdate()
        {
            var randomArray = World.GetExistingSystem<RandomizerSystem>().RandomArray;

            var timedelta = Time.DeltaTime;

            NativeArray<RoadConnectionData> roadConnectionsLocal = new NativeArray<RoadConnectionData>(roadConnections, Allocator.TempJob);

            Entities
                .WithNativeDisableParallelForRestriction(randomArray)
                .ForEach((int nativeThreadIndex, ref Translation position, ref Rotation rotation, ref VehicleComponent vehicleData) =>
                {
                    if (math.distance(position.Value, vehicleData.target) == 0 || math.distance(vehicleData.target, new float3(0, 0, 0)) == 0)
                    {
                        NativeArray<RoadConnectionData> myData = new NativeArray<RoadConnectionData>(roadConnectionsLocal, Allocator.Temp);
                        NativeList<float3> potentialTargets = new NativeList<float3>(Allocator.Temp);
                        for (int i = 0; i < myData.Length; i++)
                        {
                            if (math.distance(position.Value, myData[i].startPosition) == 0)
                            {
                                potentialTargets.Add(myData[i].endPosition);
                            }
                        }

                        if (potentialTargets.Length > 0)
                        {
                            var radom = randomArray[nativeThreadIndex];
                            int myRandomId = radom.NextInt(0, potentialTargets.Length);
                            randomArray[nativeThreadIndex] = radom;

                            float3 chosenTarget = potentialTargets[myRandomId];
                            vehicleData.target = chosenTarget;
                        }
                        potentialTargets.Dispose();
                        myData.Dispose();
                    }
                    else
                    {
                        position.Value = Float3Extensions.MoveTowards(position.Value, vehicleData.target, vehicleData.speed * timedelta);
                        if (math.length(vehicleData.target - position.Value) != 0)
                            rotation.Value = quaternion.LookRotation(vehicleData.target - position.Value, new float3(0, 1, 0));
                    }
                }).ScheduleParallel();

            Dependency.Complete();

            roadConnectionsLocal.Dispose();
        }
    }
}