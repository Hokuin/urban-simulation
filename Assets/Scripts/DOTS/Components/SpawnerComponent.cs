﻿using Unity.Entities;

namespace UrbanWorld.Generators
{
    public struct SpawnerComponent : IComponentData
    {
        public int citizensToSpawn;
        public int vehiclesToSpawn;
    }
}