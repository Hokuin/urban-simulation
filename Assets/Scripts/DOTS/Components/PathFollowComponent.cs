﻿using Unity.Entities;

namespace UrbanWorld
{
    [GenerateAuthoringComponent]
    public struct PathFollowComponent : IComponentData
    {
        public int pathIndex;
    }
}
