﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

namespace UrbanWorld.Pathfinding
{
    public class PavementPathfinding : MonoBehaviour
    {
        public PavementNetwork pavementNetwork;
        
        public static PavementPathfinding Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public Vector3 GetRandomTarget()
        {
            return pavementNetwork.GetRandomNode().position;
        }

        public Vector3[] CalculatePath(Vector3 start, Vector3 target)
        {
            NodeOOP[,] nodes = pavementNetwork.nodes;
            for (int i = 0; i < pavementNetwork.sizeX; i++)
                for (int j = 0; j < pavementNetwork.sizeZ; j++)
                {
                    nodes[i, j].gCost = uint.MaxValue;
                    nodes[i, j].parent = null;
                }

            NodeOOP startNode = pavementNetwork.GetNodeByPosition(start);
            startNode.gCost = 0;
            NodeOOP targetNode = pavementNetwork.GetNodeByPosition(target);

            if (startNode == targetNode)
                return new Vector3[] { targetNode.position };

            HashSet<NodeOOP> openedSet = new HashSet<NodeOOP>();
            HashSet<NodeOOP> closedSet = new HashSet<NodeOOP>();

            //add the starting node to the open list
            openedSet.Add(startNode);

            //repeat the following
            while (openedSet.Count > 0)
            {
                //look for the lowest f cost node on the open list
                NodeOOP currentNode = openedSet.First();
                foreach (var opened in openedSet)
                {
                    if (opened.fCost < currentNode.fCost)
                    {
                        currentNode = opened;
                    }
                }
                if (currentNode == targetNode)
                    return RetracePath(targetNode);

                //switch it to the closed list
                openedSet.Remove(currentNode);
                closedSet.Add(currentNode);

                //for each of the adjacents to current node
                foreach (NodeOOP neighbour in pavementNetwork.GetNeighbours(currentNode))
                {

                    //if it is not walkable or on closed list, ignore it
                    if (closedSet.Contains(neighbour))
                        continue;
                    if (!neighbour.walkable)
                    {
                        closedSet.Add(neighbour);
                        continue;
                    }

                    uint tentativeCost = currentNode.gCost + 1;

                    if (tentativeCost < neighbour.gCost)
                    {
                        neighbour.parent = currentNode;
                        neighbour.gCost = tentativeCost;
                        neighbour.hCost = pavementNetwork.GetDistance(neighbour, targetNode);

                        openedSet.Add(neighbour);
                    }
                }
            }

            return new Vector3[0];
        }

        private Vector3[] RetracePath(NodeOOP endNode)
        {
            List<NodeOOP> path = new List<NodeOOP>();
            NodeOOP currentNode = endNode;
            Vector3 dir = new Vector3(-1, -1, -1);
            while (currentNode.parent != null)
            {
                path.Add(currentNode);
                currentNode = currentNode.parent;
            }
            path.Reverse();
            return path.Select(t => t.position).ToArray();
        }
    }
}