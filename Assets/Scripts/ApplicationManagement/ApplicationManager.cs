﻿using UnityEngine;
using UnityEngine.Events;
using UrbanWorld;
using UrbanWorld.Generators;

namespace AppliacationManagement
{
    public class ApplicationManager : MonoBehaviour
    {
        private Cell[,] cells;
        public int SizeX { get; private set; } = 0;
        public int SizeZ { get; private set; } = 0;
        public string SimulationName { get; private set; } = "";
        public bool IsSimulationLoadedFromFile { get; private set; } = false;

        public bool IsCameraDisabled { get; private set; } = false;
        public int SimulationMode { get; private set; } = 0; //stands for OOP

        public UnityAction OnNodesPushed;

        public LevelVisualizer.CombiningMode CombiningMode { get; private set; } = LevelVisualizer.CombiningMode.noneCombining;

        private void Awake()
        {
            cells = new Cell[0,0];
        }

        public void PushCells(Cell[,] cells, string simulationName, bool isLoadedFromFile)
        {
            this.cells = cells;
            SizeX = cells.GetLength(0);
            SizeZ = cells.GetLength(1);
            this.SimulationName = simulationName;
            IsSimulationLoadedFromFile = isLoadedFromFile;
            
            if (SizeX + SizeZ > 0)
                OnNodesPushed?.Invoke();
        }

        public Cell[,] PullCells() => cells;

        public void SetCombiningMode(LevelVisualizer.CombiningMode combiningMode)
        {
            CombiningMode = combiningMode;
        }

        internal void SetSimulationMode(int simulationMode)
        {
            SimulationMode = simulationMode;
        }

        internal void SetCameraMode(bool disableCamera)
        {
            IsCameraDisabled = disableCamera;
        }
    }
}