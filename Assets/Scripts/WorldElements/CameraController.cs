using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;

    [HideInInspector] public Camera myCamera;

    private void Awake()
    {
        myCamera = GetComponentInChildren<Camera>();
    }

    private void Update()
    {
        transform.Rotate(Vector3.up, movementSpeed * Time.deltaTime);
    }

    public void SetPosition(Vector2 centerNodeCoords, float radius)
    {
        transform.position = new Vector3(centerNodeCoords.x, 0, centerNodeCoords.y);
        
        myCamera.transform.position = transform.position + new Vector3(radius, radius * 1.5f, 0);

        myCamera.transform.LookAt(transform.position);
    }
}
