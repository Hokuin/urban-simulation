﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//using Unity.Entities;
//using Unity.Mathematics;

namespace UrbanWorld
{
    public class Road : Placeable/*, IConvertGameObjectToEntity*/
    {
        [SerializeField] private bool hasPavementUpSide = false;
        [SerializeField] private bool hasPavementDownSide = false;
        [SerializeField] private bool hasPavementLeftSide = false;
        [SerializeField] private bool hasPavementRightSide = false;
        [SerializeField] private bool hasPavementUpLeftCorner = false;
        [SerializeField] private bool hasPavementUpRightCorner = false;
        [SerializeField] private bool hasPavementDownLeftCorner = false;
        [SerializeField] private bool hasPavementDownRightCorner = false;

        [SerializeField] private bool hasCenterCrossingUpDown = false;
        [SerializeField] private bool hasCenterCrossingLeftRight = false;
        [SerializeField] private bool hasUpperCrossingLeftRight = false;
        [SerializeField] private bool hasBottomCrossingLeftRight = false;
        [SerializeField] private bool hasLeftCrossingUpDown = false;
        [SerializeField] private bool hasRightCrossingUpDown = false;

        public override Vector3[] GetWalkableCoords()
        {
            var result = new HashSet<Vector3>();

            if (hasPavementUpSide) result.Add(transform.position + new Vector3(0, 0, 0.5f));
            if (hasPavementDownSide) result.Add(transform.position + new Vector3(0, 0, -0.5f));
            if (hasPavementLeftSide) result.Add(transform.position + new Vector3(-0.5f, 0, 0));
            if (hasPavementRightSide) result.Add(transform.position + new Vector3(0.5f, 0, 0));
            if (hasPavementUpLeftCorner) result.Add(transform.position + new Vector3(-0.5f, 0, 0.5f));
            if (hasPavementUpRightCorner) result.Add(transform.position + new Vector3(0.5f, 0, 0.5f));
            if (hasPavementDownLeftCorner) result.Add(transform.position + new Vector3(-0.5f, 0, -0.5f));
            if (hasPavementDownRightCorner) result.Add(transform.position + new Vector3(0.5f, 0, -0.5f));

            if (hasCenterCrossingUpDown)
            {
                result.Add(transform.position + new Vector3(0, 0, 0));
            }

            if (hasCenterCrossingLeftRight)
            {
                result.Add(transform.position + new Vector3(0, 0, 0));
            }

            if (hasUpperCrossingLeftRight)
            {
                result.Add(transform.position + new Vector3(0, 0, 0.4f));
            }

            if (hasBottomCrossingLeftRight)
            {
                result.Add(transform.position + new Vector3(0, 0, -0.4f));
            }

            if (hasLeftCrossingUpDown)
            {
                result.Add(transform.position + new Vector3(-0.4f, 0, -0));
            }
            if (hasRightCrossingUpDown)
            {
                result.Add(transform.position + new Vector3(0.4f, 0, -0));
            }

            return result.ToArray();
        }

        /*
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            DynamicBuffer<CitizenWalkableCoordsBuffer> citizenWalkableCoordsBuffer = dstManager.AddBuffer<CitizenWalkableCoordsBuffer>(entity);
            var coordsToAdd = GetWalkableCoords();
            for (int i = 0; i < coordsToAdd.Length; i++)
                citizenWalkableCoordsBuffer.Add((float3)coordsToAdd[i]);
                //citizenWalkableCoordsBuffer.Add(new CitizenWalkableCoordsBuffer { walkableCoords = coordsToAdd[i] });
                
            DynamicBuffer<VehicleConnectionsBuffer> vehicleConnectionsBuffer = dstManager.AddBuffer<VehicleConnectionsBuffer>(entity);
            var roadConnections = GetInfoAboutConnectors();
            for (int i = 0; i < roadConnections.Length; i++)
                vehicleConnectionsBuffer.Add(roadConnections[i]);
                //vehicleConnectionsBuffer.Add(new VehicleConnectionsBuffer { connectorsData = roadConnections[i] });
            
        }
        
        private RoadConnectionData[] GetInfoAboutConnectors()
        {
            var result = new HashSet<RoadConnectionData>();

            var connectors = GetComponentsInChildren<Connector>().Where(c => c.type == ConnectorType.In);

            foreach (var connector in connectors)
            {
                foreach (var path in connector.paths)
                {
                    var connection = new RoadConnectionData
                    {
                        startPosition = connector.transform.position,
                        endPosition = path.connectorOut.transform.position
                    };
                    result.Add(connection);
                }
            }
            return result.ToArray();
        }
        */
    }
}