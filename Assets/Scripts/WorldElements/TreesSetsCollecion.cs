public class TreesSetsCollecion : DecorationsCollection
{
    public override void ShowRandomDecorations()
    {
        for (int i = 0; i < decorationSet.Length; i++)
            decorationSet[i].TryShowRandomDecoration();
    }
}
