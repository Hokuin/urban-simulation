﻿using System.Collections.Generic;
using UnityEngine;
using UrbanWorld;

namespace UrbanWorld.Generators
{
    [System.Serializable]
    public class RoadVariantsSet
    {
        private List<Road> variants = new List<Road>();
        public void AddVariant(Road r)
        {
            variants.Add(r);
        }

        public Road GetRandomVariant()
        {
            return variants[Random.Range(0, variants.Count)];
        }
    }
}