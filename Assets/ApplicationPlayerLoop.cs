using System.Collections.Generic;
using UnityEngine;

namespace UrbanWorld.Simulation
{
    public class ApplicationPlayerLoop : MonoBehaviour
    {
        public static ApplicationPlayerLoop Instance;
        private HashSet<IUpdateable> updateables = new HashSet<IUpdateable>();

        private void Awake()
        {
            Application.targetFrameRate = 300;
        }

        private void Start()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }

        private void Update()
        {
            foreach (var u in updateables)
                u.UpdateMe();
        }

        public void AddUpdateable(IUpdateable updateable)
        {
            updateables.Add(updateable);
        }

        public void RemoveUpdateable(IUpdateable updateable)
        {
            updateables.Remove(updateable);
        }
    }

    public interface IUpdateable
    {
        public void UpdateMe();
    }
}