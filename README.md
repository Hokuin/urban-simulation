"Urban simulation" is a project carried out as part of my engineering thesis, entitled: "Urban space simulation on Unity engine using DOTS technology".
According to the theme, the application is to allow simulation of urban space using pre-released DOTS technology, specifically:
- ECS ("Entity Component System")
- Jobs
- Burst Compiler
The purpose of the engineering thesis is to evaluate the performance of the simulation compared to classical object-oriented programming.